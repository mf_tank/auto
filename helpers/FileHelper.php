<?php

namespace app\helpers;
use yii\helpers\BaseFileHelper;

/**
 * Class FileHelper
 * @package app\helpers
 */
class FileHelper extends BaseFileHelper
{
    /**
     * Удаление файла
     * @param $file
     */
    public static function deleteFile($file)
    {
        if(file_exists($file)) {
            unlink($file);
        }
    }
}
