<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Регистрация';
?>


<h1>
    <?= Html::encode($this->title) ?>
</h1>

<div class="row">
    <div class="col-md-6">
        <?php $form = ActiveForm::begin([
            'layout' => 'default'
        ]); ?>

        <?php echo $form->field($model, 'username');?>

        <?php
            echo $form->field($model, 'password')->passwordInput();

            echo $form->field($model, 'password_repeat')->passwordInput();
        ?>

        <div class="row">
            <div class="col-md-4">
                <?= Html::submitButton('Зарегистрироваться', [
                        'class' => 'btn btn-primary'
                ])?>
            </div>
            <div class="col-md-4">
                <?= Html::a('Авторизация', '/', []);?>
            </div>
            <div class="col-md-4">
                <?= Html::a('Забыли пароль', 'forget', []);?>
            </div>
        </div>
        <?php ActiveForm::end();?>
    </div>
</div>
