<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Забыли пароль';
?>


<h1>
    <?= Html::encode($this->title) ?>
</h1>

<div class="row">
    <div class="col-md-6">
        <?php $form = ActiveForm::begin([]); ?>

        <?= $form->field($model, 'username') ?>

        <div class="row">
            <div class="col-md-3">
                <?= Html::submitButton('Отправить', [
                    'class' => 'btn btn-primary'
                ]) ?>
            </div>
            <?/*
            <div class="col-md-3">
                <?= Html::a('Регистрация', 'reg', []);?>
            </div>
            */?>
            <div class="col-md-3">
                <?= Html::a('Авторизация', '/', []);?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

