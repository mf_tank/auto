<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Авторизация';
?>


<h1>
    <?= Html::encode($this->title) ?>
</h1>

<div class="row">
    <div class="col-md-6">
        <?php $form = ActiveForm::begin([]); ?>

        <?= $form->field($model, 'username') ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox() ?>


        <div class="row">
            <div class="col-md-3">
                <?= Html::submitButton('Войти', [
                        'class' => 'btn btn-primary'
                ]) ?>
            </div>
            <?/*
            <div class="col-md-3">
                <?= Html::a('Регистрация', 'reg', []);?>
            </div>
            */?>
            <div class="col-md-3">
                <?= Html::a('Забыли пароль', 'forget', []);?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

