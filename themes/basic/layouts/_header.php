<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

NavBar::begin([ // отрываем виджет
    'brandLabel' => '<img width="50px" src="/images/logo.jpg" alt="likecat.net">', // название организации
    'brandUrl' => Yii::$app->homeUrl, // ссылка на главную страницу сайта
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top', // стили главной панели
    ],
]);
    echo Nav::widget([
        'items' => [
            [
                'label' => 'Накрутка',
                'url' => ['/tasks'],
            ],
            [
                'label' => 'Автонакрутка',
                'url' => ['/autocheat'],
            ],
            [
                'label' => 'Баланс',
                'url' => ['balance'],
            ],
            [
                'label' => 'Новости',
                'url' => ['/news'],
            ],
            [
                'label' => 'Профиль',
                'url' => ['/profile'],
            ],

            [
                'label' => 'Выход (' . Yii::$app->user->identity->username . ')',
                'url' => ['site/logout'],
            ],
        ],
        'options' => ['class' => 'navbar-nav navbar-right'],
    ]);
NavBar::end(); // закрываем виджет
