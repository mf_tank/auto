<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "article_attributes".
 *
 * @property int $supplierid
 * @property string $datasupplierarticlenumber
 * @property int $number
 * @property string $description
 * @property string $displaytitle
 * @property string $displayvalue
 */
class ArticleAttributes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article_attributes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['supplierid', 'datasupplierarticlenumber', 'id', 'displaytitle', 'displayvalue'], 'required'],
            [['supplierid', 'number'], 'integer'],
            [['datasupplierarticlenumber'], 'string', 'max' => 32],
            [['description', 'displaytitle'], 'string', 'max' => 128],
            [['displayvalue'], 'string', 'max' => 4000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'supplierid' => 'Supplierid',
            'datasupplierarticlenumber' => 'Datasupplierarticlenumber',
            'number' => 'number',
            'description' => 'Description',
            'displaytitle' => 'Displaytitle',
            'displayvalue' => 'Displayvalue',
        ];
    }
}
