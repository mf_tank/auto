<?php

namespace app\models;

use app\components\BaseARecord;
use Yii;

/**
 * This is the model class for table "yii2_store".
 *
 * @property int $id
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 * @property int $author_id
 * @property int $updater_id
 *
 * @property PriceStore[] $priceStores
 * @property Price[] $prices
 */
class Store extends BaseARecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii2_store';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'author_id' => 'Author ID',
            'updater_id' => 'Updater ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceStores()
    {
        return $this->hasMany(PriceStore::className(), ['store_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasMany(Price::className(), ['id' => 'price_id'])->viaTable('yii2_price_store', ['store_id' => 'id']);
    }
}
