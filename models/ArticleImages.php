<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "article_images".
 *
 * @property int $supplierId
 * @property string $DataSupplierArticleNumber
 * @property string $AdditionalDescription
 * @property string $Description
 * @property string $DocumentName
 * @property string $DocumentType
 * @property int $NormedDescriptionID
 * @property string $PictureName
 * @property string $ShowImmediately
 */
class ArticleImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['supplierId', 'DataSupplierArticleNumber', 'AdditionalDescription', 'Description', 'DocumentName', 'DocumentType', 'NormedDescriptionID', 'PictureName', 'ShowImmediately'], 'required'],
            [['supplierId', 'NormedDescriptionID'], 'integer'],
            [['ShowImmediately'], 'string'],
            [['DataSupplierArticleNumber'], 'string', 'max' => 32],
            [['AdditionalDescription', 'Description', 'PictureName'], 'string', 'max' => 64],
            [['DocumentName'], 'string', 'max' => 128],
            [['DocumentType'], 'string', 'max' => 8],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'supplierId' => 'Supplier ID',
            'DataSupplierArticleNumber' => 'Data Supplier Article Number',
            'AdditionalDescription' => 'Additional Description',
            'Description' => 'Description',
            'DocumentName' => 'Document Name',
            'DocumentType' => 'Document Type',
            'NormedDescriptionID' => 'Normed Description ID',
            'PictureName' => 'Picture Name',
            'ShowImmediately' => 'Show Immediately',
        ];
    }
}
