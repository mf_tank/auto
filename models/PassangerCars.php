<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "passanger_cars".
 *
 * @property int $id
 * @property string $canbedisplayed
 * @property string $constructioninterval
 * @property string $description
 * @property string $fulldescription
 * @property string $haslink
 * @property string $isaxle
 * @property string $iscommercialvehicle
 * @property string $iscvmanufacturerid
 * @property string $isengine
 * @property string $ismotorbike
 * @property string $ispassengercar
 * @property string $istransporter
 * @property int $modelid
 */
class PassangerCars extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'passanger_cars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'modelid'], 'integer'],
            [['canbedisplayed', 'haslink', 'isaxle', 'iscommercialvehicle', 'iscvmanufacturerid', 'isengine', 'ismotorbike', 'ispassengercar', 'istransporter'], 'string'],
            [['constructioninterval'], 'string', 'max' => 24],
            [['description'], 'string', 'max' => 128],
            [['fulldescription'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'canbedisplayed' => 'Canbedisplayed',
            'constructioninterval' => 'Constructioninterval',
            'description' => 'Description',
            'fulldescription' => 'Fulldescription',
            'haslink' => 'Haslink',
            'isaxle' => 'Isaxle',
            'iscommercialvehicle' => 'Iscommercialvehicle',
            'iscvmanufacturerid' => 'Iscvmanufacturerid',
            'isengine' => 'Isengine',
            'ismotorbike' => 'Ismotorbike',
            'ispassengercar' => 'Ispassengercar',
            'istransporter' => 'Istransporter',
            'modelid' => 'Modelid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassangerCarAttributes()
    {
        return $this->hasMany(PassangerCarAttributes::className(), ['passangercarid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Models::className(), ['id' => 'modelid']);
    }

}
