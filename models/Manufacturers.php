<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "manufacturers".
 *
 * @property int $id
 * @property string $canbedisplayed
 * @property string $description
 * @property string $fulldescription
 * @property string $haslink
 * @property string $isaxle
 * @property string $iscommercialvehicle
 * @property string $isengine
 * @property string $ismotorbike
 * @property string $ispassengercar
 * @property string $istransporter
 * @property string $isvgl
 * @property string $matchcode
 */
class Manufacturers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manufacturers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'isaxle', 'iscommercialvehicle', 'isengine', 'ismotorbike', 'ispassengercar', 'istransporter'], 'required'],
            [['id'], 'integer'],
            [['canbedisplayed', 'haslink', 'isaxle', 'iscommercialvehicle', 'isengine', 'ismotorbike', 'ispassengercar', 'istransporter', 'isvgl'], 'string'],
            [['description', 'fulldescription', 'matchcode'], 'string', 'max' => 64],
            [['id', 'isaxle', 'iscommercialvehicle', 'isengine', 'ismotorbike', 'ispassengercar', 'istransporter'], 'unique', 'targetAttribute' => ['id', 'isaxle', 'iscommercialvehicle', 'isengine', 'ismotorbike', 'ispassengercar', 'istransporter']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'canbedisplayed' => 'Canbedisplayed',
            'description' => 'Description',
            'fulldescription' => 'Fulldescription',
            'haslink' => 'Haslink',
            'isaxle' => 'Isaxle',
            'iscommercialvehicle' => 'Iscommercialvehicle',
            'isengine' => 'Isengine',
            'ismotorbike' => 'Ismotorbike',
            'ispassengercar' => 'Ispassengercar',
            'istransporter' => 'Istransporter',
            'isvgl' => 'Isvgl',
            'matchcode' => 'Matchcode',
        ];
    }
}
