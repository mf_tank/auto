<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "article_links".
 *
 * @property int $supplierid
 * @property int $productid
 * @property int $linkagetypeid
 * @property int $linkageid
 * @property string $datasupplierarticlenumber
 */
class ArticleLinks extends \yii\db\ActiveRecord
{

    public $part_number;
    public $supplier_name;
    public $product_name;
    public $modification_id;
    public $section_id;
    public $price;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article_links';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['supplierid', 'productid', 'linkagetypeid', 'linkageid', 'datasupplierarticlenumber'], 'required'],
            [['supplierid', 'productid', 'linkagetypeid', 'linkageid'], 'integer'],
            [['datasupplierarticlenumber'], 'string', 'max' => 32],
            [['supplierid', 'productid', 'linkagetypeid', 'linkageid', 'datasupplierarticlenumber'], 'unique', 'targetAttribute' => ['supplierid', 'productid', 'linkagetypeid', 'linkageid', 'datasupplierarticlenumber']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'supplierid' => 'Supplierid',
            'productid' => 'Productid',
            'linkagetypeid' => 'Linkagetypeid',
            'linkageid' => 'Linkageid',
            'datasupplierarticlenumber' => 'Datasupplierarticlenumber',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassangerCarPds()
    {
        return $this->hasMany(PassangerCarPds::className(), ['supplierid' => 'supplierid']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuppliers()
    {
        return $this->hasMany(Suppliers::className(), ['id' => 'supplierid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassangerCarPrd()
    {
        return $this->hasMany(PassangerCarPrd::className(), ['id' => 'productid']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArmtek()
    {
        return $this->hasMany(Price::className(), ['article' => 'datasupplierarticlenumber']);
    }
}
