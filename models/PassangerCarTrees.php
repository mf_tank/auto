<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "passanger_car_trees".
 *
 * @property int $passangercarid
 * @property int $searchtreeid
 * @property int $id
 * @property int $parentid
 * @property string $description
 */
class PassangerCarTrees extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'passanger_car_trees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['passangercarid', 'searchtreeid', 'number'], 'required'],
            [['passangercarid', 'searchtreeid', 'number', 'parentid'], 'integer'],
            [['description'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'passangercarid' => 'Passangercarid',
            'searchtreeid' => 'Searchtreeid',
            'number' => 'ID',
            'parentid' => 'Parentid',
            'description' => 'Description',
        ];
    }
}
