<?php

namespace app\models;

use app\components\BaseARecord;
use Yii;

/**
 * This is the model class for table "yii2_order".
 *
 * @property int $id
 * @property string $str
 * @property string $name
 * @property string $phone
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $author_id
 * @property int $updater_id
 */
class Order extends BaseARecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii2_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['str', 'name', 'phone'], 'required'],
            [['status', 'created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['str', 'name', 'phone'], 'string', 'max' => 255],
            [['str'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'str' => 'Str',
            'name' => 'Name',
            'phone' => 'Phone',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'author_id' => 'Author ID',
            'updater_id' => 'Updater ID',
        ];
    }
}
