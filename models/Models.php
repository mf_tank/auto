<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "models".
 *
 * @property int $id
 * @property string $canbedisplayed
 * @property string $constructioninterval
 * @property string $description
 * @property string $fulldescription
 * @property string $haslink
 * @property string $isaxle
 * @property string $iscommercialvehicle
 * @property string $isengine
 * @property string $ismotorbike
 * @property string $ispassengercar
 * @property string $istransporter
 * @property int $manufacturerid
 */
class Models extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'models';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'isaxle', 'iscommercialvehicle', 'isengine', 'ismotorbike', 'ispassengercar', 'istransporter'], 'required'],
            [['id', 'manufacturerid'], 'integer'],
            [['canbedisplayed', 'haslink', 'isaxle', 'iscommercialvehicle', 'isengine', 'ismotorbike', 'ispassengercar', 'istransporter'], 'string'],
            [['constructioninterval'], 'string', 'max' => 24],
            [['description', 'fulldescription'], 'string', 'max' => 128],
            [['id', 'isaxle', 'iscommercialvehicle', 'isengine', 'ismotorbike', 'ispassengercar', 'istransporter'], 'unique', 'targetAttribute' => ['id', 'isaxle', 'iscommercialvehicle', 'isengine', 'ismotorbike', 'ispassengercar', 'istransporter']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'canbedisplayed' => 'Canbedisplayed',
            'constructioninterval' => 'Constructioninterval',
            'description' => 'Description',
            'fulldescription' => 'Fulldescription',
            'haslink' => 'Haslink',
            'isaxle' => 'Isaxle',
            'iscommercialvehicle' => 'Iscommercialvehicle',
            'isengine' => 'Isengine',
            'ismotorbike' => 'Ismotorbike',
            'ispassengercar' => 'Ispassengercar',
            'istransporter' => 'Istransporter',
            'manufacturerid' => 'Manufacturerid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(Manufacturers::className(), ['id' => 'manufacturerid']);
    }
}
