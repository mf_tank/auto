<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "passanger_car_prd".
 *
 * @property int $id
 * @property string $assemblygroupdescription
 * @property string $description
 * @property string $normalizeddescription
 * @property string $usagedescription
 */
class PassangerCarPrd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'passanger_car_prd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'assemblygroupdescription', 'description', 'normalizeddescription', 'usagedescription'], 'required'],
            [['id'], 'integer'],
            [['assemblygroupdescription', 'description', 'normalizeddescription', 'usagedescription'], 'string', 'max' => 128],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'assemblygroupdescription' => 'Assemblygroupdescription',
            'description' => 'Description',
            'normalizeddescription' => 'Normalizeddescription',
            'usagedescription' => 'Usagedescription',
        ];
    }
}
