<?php

namespace app\models;

use app\components\BaseARecord;
use Yii;

/**
 * This is the model class for table "yii2_basket".
 *
 * @property int $id
 * @property string $str
 * @property string $article
 * @property string $section_id
 * @property string $modification_id
 * @property int $count
 * @property int $created_at
 * @property int $updated_at
 * @property int $author_id
 * @property int $updater_id
 */
class Basket extends BaseARecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii2_basket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['str', 'article', 'section_id', 'modification_id', 'count', 'price'], 'required'],
            [['count', 'created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['str', 'article', 'section_id', 'modification_id', 'price'], 'string', 'max' => 255],
            [['str', 'article', 'section_id', 'modification_id'], 'unique', 'targetAttribute' => ['str', 'article', 'section_id', 'modification_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'str' => 'Str',
            'article' => 'Article',
            'section_id' => 'Section ID',
            'modification_id' => 'Modification ID',
            'count' => 'Count',
            'price' => 'Цена',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'author_id' => 'Author ID',
            'updater_id' => 'Updater ID',
        ];
    }
}
