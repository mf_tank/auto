<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "passanger_car_attributes".
 *
 * @property int $passangercarid
 * @property string $attributegroup
 * @property string $attributetype
 * @property string $displaytitle
 * @property string $displayvalue
 */
class PassangerCarAttributes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'passanger_car_attributes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['passangercarid'], 'required'],
            [['passangercarid'], 'integer'],
            [['attributegroup', 'attributetype', 'displaytitle'], 'string', 'max' => 32],
            [['displayvalue'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'passangercarid' => 'Passangercarid',
            'attributegroup' => 'Attributegroup',
            'attributetype' => 'Attributetype',
            'displaytitle' => 'Displaytitle',
            'displayvalue' => 'Displayvalue',
        ];
    }
}
