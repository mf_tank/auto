<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "article_cross".
 *
 * @property int $manufacturerId
 * @property string $OENbr
 * @property int $SupplierId
 * @property string $PartsDataSupplierArticleNumber
 */
class ArticleCross extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article_cross';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['manufacturerId', 'OENbr', 'SupplierId', 'PartsDataSupplierArticleNumber'], 'required'],
            [['manufacturerId', 'SupplierId'], 'integer'],
            [['OENbr'], 'string', 'max' => 64],
            [['PartsDataSupplierArticleNumber'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'manufacturerId' => 'Manufacturer ID',
            'OENbr' => 'Oe Nbr',
            'SupplierId' => 'Supplier ID',
            'PartsDataSupplierArticleNumber' => 'Parts Data Supplier Article Number',
        ];
    }
}
