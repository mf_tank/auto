<?php
namespace app\models\sphinx;


use yii\sphinx\ActiveRecord;


/**
 * Class ManufacturersIndex
 * @package app\models\sphinx
 */
class ManufacturersIndex extends ActiveRecord
{

    /**
     * @return string
     */
    public static function indexName()
    {
        return 'sphinx_index_manufactures';
    }
}
