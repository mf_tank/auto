<?php
namespace app\models\sphinx;


use yii\sphinx\ActiveRecord;


/**
 * Class ArticleAttributesIndex
 * @package app\models\sphinx
 */
class ArticleAttributesIndex extends ActiveRecord
{

    /**
     * @return string
     */
    public static function indexName()
    {
        return 'sphinx_index_article_attributes';
    }
}
