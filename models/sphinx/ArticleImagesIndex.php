<?php
namespace app\models\sphinx;


use yii\sphinx\ActiveRecord;


/**
 * Class ArticleImagesIndex
 * @package app\models\sphinx
 */
class ArticleImagesIndex extends ActiveRecord
{

    /**
     * @return string
     */
    public static function indexName()
    {
        return 'sphinx_index_article_images';
    }
}
