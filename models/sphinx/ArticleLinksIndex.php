<?php
namespace app\models\sphinx;


use yii\sphinx\ActiveRecord;


/**
 * Class ArticleLinksIndex
 * @package app\models\sphinx
 */
class ArticleLinksIndex extends ActiveRecord
{

    /**
     * @return string
     */
    public static function indexName()
    {
        return 'sphinx_index_article_links';
    }
}
