<?php
namespace app\models\sphinx;


use yii\sphinx\ActiveRecord;


/**
 * Class ArticleOeIndex
 * @package app\models\sphinx
 */
class ArticleOeIndex extends ActiveRecord
{

    /**
     * @return string
     */
    public static function indexName()
    {
        return 'sphinx_index_article_oe';
    }
}
