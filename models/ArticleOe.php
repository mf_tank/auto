<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "article_oe".
 *
 * @property int $supplierid
 * @property string $datasupplierarticlenumber
 * @property string $IsAdditive
 * @property string $OENbr
 * @property int $manufacturerId
 */
class ArticleOe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article_oe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['supplierid', 'datasupplierarticlenumber', 'IsAdditive', 'OENbr', 'manufacturerId'], 'required'],
            [['supplierid', 'manufacturerId'], 'integer'],
            [['IsAdditive'], 'string'],
            [['datasupplierarticlenumber'], 'string', 'max' => 32],
            [['OENbr'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'supplierid' => 'Supplierid',
            'datasupplierarticlenumber' => 'Datasupplierarticlenumber',
            'IsAdditive' => 'Is Additive',
            'OENbr' => 'Oe Nbr',
            'manufacturerId' => 'Manufacturer ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturers()
    {
        return $this->hasMany(Manufacturers::className(), ['id' => 'manufacturerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCross()
    {
        return $this->hasMany(ArticleCross::className(), ['OENbr' => 'OENbr']);
    }


    public function getSuppliers()
    {
        return $this->hasMany(Suppliers::className(), ['id' => 'SupplierId'])
            ->via('articleCross');
    }
}
