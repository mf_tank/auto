<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "suppliers".
 *
 * @property int $id
 * @property int $dataversion
 * @property string $description
 * @property string $matchcode
 * @property int $nbrofarticles
 * @property string $hasnewversionarticles
 */
class Suppliers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'suppliers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'dataversion', 'nbrofarticles'], 'integer'],
            [['hasnewversionarticles'], 'string'],
            [['description', 'matchcode'], 'string', 'max' => 32],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dataversion' => 'Dataversion',
            'description' => 'Description',
            'matchcode' => 'Matchcode',
            'nbrofarticles' => 'Nbrofarticles',
            'hasnewversionarticles' => 'Hasnewversionarticles',
        ];
    }
}
