<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "passanger_car_pds".
 *
 * @property int $passangercarid
 * @property int $nodeid
 * @property int $productid
 * @property int $supplierid
 */
class PassangerCarPds extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'passanger_car_pds';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['passangercarid', 'nodeid', 'productid', 'supplierid'], 'required'],
            [['passangercarid', 'nodeid', 'productid', 'supplierid'], 'integer'],
            [['passangercarid', 'nodeid', 'productid', 'supplierid'], 'unique', 'targetAttribute' => ['passangercarid', 'nodeid', 'productid', 'supplierid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'passangercarid' => 'Passangercarid',
            'nodeid' => 'Nodeid',
            'productid' => 'Productid',
            'supplierid' => 'Supplierid',
        ];
    }

}
