<?php

namespace app\models;

use app\components\BaseARecord;
use Yii;

/**
 * This is the model class for table "yii2_price".
 *
 * @property int $id
 * @property string $article
 * @property string $brand
 * @property string $code
 * @property string $name
 * @property string $price
 * @property int $created_at
 * @property int $updated_at
 * @property int $author_id
 * @property int $updater_id
 *
 * @property PriceStore[] $priceStores
 * @property Store[] $stores
 */
class Price extends BaseARecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii2_price';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['article', 'brand', 'code', 'name', 'price'], 'required'],
            [['price'], 'number'],
            [['created_at', 'updated_at', 'author_id', 'updater_id'], 'integer'],
            [['article', 'brand', 'code', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article' => 'Article',
            'brand' => 'Brand',
            'code' => 'Code',
            'name' => 'Name',
            'price' => 'Price',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'author_id' => 'Author ID',
            'updater_id' => 'Updater ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceStores()
    {
        return $this->hasMany(PriceStore::className(), ['price_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStores()
    {
        return $this->hasMany(Store::className(), ['id' => 'store_id'])
            ->viaTable('yii2_price_store', ['price_id' => 'id']);
    }
}
