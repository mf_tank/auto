<?php

namespace app\components;

use Yii;

use yii\web\Controller;
use app\utils\LanguageUtil;

/**
 * Class BaseController
 * @package app\modules\admin\components
 */
abstract class BaseController extends Controller
{

    /**
     * Шаблон
     * @var string
     */
    public $layout = 'main';

    public $lang = 'ru';



    /**
     * @param \yii\base\Action $event
     * @return bool
     */
    public function beforeAction($event)
    {
        return parent::beforeAction($event);
    }
}
