<?php
namespace app\modules\api\controllers;


use app\modules\api\components\RestController;


/**
 * Class FeedbackController
 * @package app\modules\api\controllers
 */
class FeedbackController extends RestController
{

    /**
     * @var string
     */
    public $modelClass = 'app\modules\api\models\FeedbackRest';


    /**
     * Методы
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset(
            $actions['index'],
            $actions['view'],
            $actions['update'],
            $actions['delete'],
            $actions['options']
        );

        return $actions;
    }


}
