<?php

namespace app\modules\api\controllers;


use app\modules\api\components\RestController;


/**
 * Class OrdersController
 * @package app\modules\api\controllers
 */
class OrdersController extends RestController
{

    /**
     * @var string
     */
    public $modelClass = 'app\modules\api\models\OrderRest';


    /**
     * Методы
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset(
            $actions['index'],
            $actions['view'],
            $actions['update'],
            $actions['delete'],
            $actions['options']
        );

        return $actions;
    }


}
