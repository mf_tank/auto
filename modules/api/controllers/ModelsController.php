<?php
namespace app\modules\api\controllers;


use Yii;
use yii\helpers\ArrayHelper;
use app\modules\api\components\RestController;
use app\modules\api\models\ModelsRest;
use app\modules\api\models\ManufacturersRest;
use yii\web\HttpException;

/**
 * @brief Модели машин //spinx
 * Class ModelsController
 * @package app\modules\api\controllers
 */
class ModelsController extends RestController
{

    /**
     * @var string
     */
    public $modelClass = 'app\modules\api\models\ModelsRest';


    /**
     * Методы
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset(
            $actions['index'],
            $actions['view'],
            $actions['create'],
            $actions['update'],
            $actions['delete'],
            $actions['options']
        );

        return $actions;
    }


    /**
     * @param $description
     * @return array
     * @throws HttpException
     */
    public function actionPassengers($description)
    {
        $brand = ManufacturersRest::getBrandName($description);
        if($brand === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        $models = ModelsRest::getModels($brand->id);

        return ArrayHelper::toArray($models, [
            'app\modules\api\models\ModelRest' => [
                'id',
                'description',
                'fulldescription',
                'constructioninterval'
            ],
        ]);
    }


    /**
     * @param $brand_id
     * @param $description
     * @return array
     * @throws HttpException
     */
    public function actionPassenger(int $brand_id, $description)
    {
        $model = ModelsRest::getModelName($brand_id, $description);
        if($model === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        return ArrayHelper::toArray($model, [
            'app\modules\api\models\ModelRest' => [
                'id',
                'description',
                'fulldescription',
                'constructioninterval'
            ],
        ]);
    }
}
