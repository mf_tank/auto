<?php
namespace app\modules\api\controllers;

use app\modules\api\models\ManufacturersRest;
use app\modules\api\models\ModelsRest;
use app\modules\api\models\PassangerCarsRest;
use app\modules\api\models\PassangerCarTreesRest;
use Yii;
use app\modules\api\components\RestController;
use yii\web\HttpException;


/**
 * @brief дерево сецкий // sphinx
 * Class SectionsController
 * @package app\modules\api\controllers
 */
class SectionsController extends RestController
{

    /**
     * @var string
     */
    public $modelClass = 'app\modules\api\models\PassangerCarTreesRest';


    /**
     * Методы
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset(
            $actions['index'],
            $actions['view'],
            $actions['create'],
            $actions['update'],
            $actions['delete'],
            $actions['options']
        );

        return $actions;
    }


    /**
     * @brief Бренды машин - id 117710 модификции лада веста 1.6 таблица passanger_cars
     * @param $param
     * @return array
     */
    public function actionMain()
    {
        $result = PassangerCarTreesRest::getSectionLevel2(117710);

        return $result;
    }

    /**
     * @param $brand_description
     * @param $model_description
     * @param $modifiction_description
     * @return array
     * @throws HttpException
     */
    public function actionAll($brand_description, $model_description, $modification_description)
    {
        $brand = ManufacturersRest::getBrandName($brand_description);
        if($brand === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        $model = ModelsRest::getModelName($brand->id, $model_description);
        if($model === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        $modification = PassangerCarsRest::getModificationName($model->id, $modification_description);
        if($modification === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        $result = PassangerCarTreesRest::getSectionAll($modification->id);

        return $result;
    }


    /**
     * @param $brand_description
     * @param $model_description
     * @param $modification_description
     * @param $section_description
     * @return array
     * @throws HttpException
     */
    public function actionChildren($brand_description, $model_description, $modification_description, $section_description)
    {
        $brand = ManufacturersRest::getBrandName($brand_description);
        if($brand === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        $model = ModelsRest::getModelName($brand->id, $model_description);
        if($model === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        $modification = PassangerCarsRest::getModificationName($model->id, $modification_description);
        if($modification === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        $section = PassangerCarTreesRest::getSectionName($modification->id, $section_description);
        if($section == null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        $result = PassangerCarTreesRest::getSectionAll($modification->id, $section->number);

        return $result;
    }


    /**
     * @param $id
     * @param $modification_id
     * @return PassangerCarTreesRest|null
     * @throws HttpException
     */
    public function actionView($id, $modification_id)
    {
        $section = PassangerCarTreesRest::getSectionId($id, $modification_id);

        if($section == null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        return $section;
    }

}
