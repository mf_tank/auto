<?php
namespace app\modules\api\controllers;

use app\modules\api\models\ManufacturersRest;
use app\modules\api\models\ModelsRest;
use app\modules\api\models\PassangerCarsRest;
use Yii;
use yii\helpers\ArrayHelper;
use app\modules\api\components\RestController;
use yii\web\HttpException;


/**
 * @brief Модификация машин
 * Class PassangerCarsController
 * @package app\modules\api\controllers
 */
class PassangerCarsController extends RestController
{

    /**
     * @var string
     */
    public $modelClass = 'app\modules\api\models\PassangerCarsRest';


    /**
     * Методы
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset(
            $actions['index'],
            $actions['view'],
            $actions['create'],
            $actions['update'],
            $actions['delete'],
            $actions['options']
        );

        return $actions;
    }


    /**
     * brief Модификации машин
     * @param $brand_description
     * @param $model_description
     * @return array
     * @throws HttpException
     */
    public function actionModifications($brand_description, $model_description)
    {
        $brand = ManufacturersRest::getBrandName($brand_description);
        if($brand === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        $model = ModelsRest::getModelName($brand->id, $model_description);
        if($model === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }


        $modifications = PassangerCarsRest::getModifications($model->id);

        return ArrayHelper::toArray($modifications, [
            'app\modules\api\models\PassangerCarsRest' => [
                'id',
                'description',
                'fulldescription',
                'constructioninterval',
                'passangerCarAttributes',
            ],
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = PassangerCarsRest::getModification($id);

        if(!$model) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        $result['modification'] = $model;
        $result['model'] = $model->model;
        if(!empty($result['model'])) {
            $result['manufacturers'] = $model->model->manufacturer;
        }


        return $result;
    }


    /**
     * @param int $model_id
     * @param $description
     * @return array
     * @throws HttpException
     */
    public function actionModification(int $model_id, string $description)
    {
        $model = PassangerCarsRest::getModificationName($model_id, $description);

        if($model === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        return ArrayHelper::toArray($model, [
            'app\modules\api\models\PassangerCarsRest' => [
                'id',
                'description',
                'fulldescription',
                'constructioninterval',
                'passangerCarAttributes',
            ],
        ]);
    }

}
