<?php
namespace app\modules\api\controllers;

use app\modules\api\models\ManufacturersRest;
use Yii;
use yii\helpers\ArrayHelper;
use app\modules\api\components\RestController;
use yii\web\HttpException;

/**
 * @brief Марки автомобилей //spinx
 * Class ManufacturersController
 * @package app\modules\api\controllers
 */
class ManufacturersController extends RestController
{

    /**
     * @var string
     */
    public $modelClass = 'app\modules\api\models\ManufacturersRest';


    /**
     * Методы
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset(
            $actions['index'],
            $actions['view'],
            $actions['create'],
            $actions['update'],
            $actions['delete'],
            $actions['options']
        );

        return $actions;
    }


    /**
     * @brief Бренды машин
     * @param $param
     * @return array
     */
    public function actionPassengers()
    {
        $models = ManufacturersRest::getBrands();

        return ArrayHelper::toArray($models, [
            'app\modules\api\models\ManufacturersRest' => [
                'id',
                'description',
                'fulldescription',
                'matchcode'
            ],
        ]);
    }

    /**
     * @param $description
     * @return array
     * @throws HttpException
     */
    public function actionPassenger($description)
    {
        $brand = ManufacturersRest::getBrandName($description);
        if($brand === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        return ArrayHelper::toArray($brand, [
            'app\modules\api\models\ManufacturersRest' => [
                'id',
                'description',
                'fulldescription',
                'matchcode'
            ],
        ]);
    }

}
