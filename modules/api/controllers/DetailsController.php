<?php
namespace app\modules\api\controllers;


use app\models\Part;
use Yii;
use yii\data\Pagination;
use app\modules\api\components\RestController;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use app\modules\api\models\ArticleAttributesRest;
use app\modules\api\models\ArticleImagesRest;
use app\modules\api\models\ArticleLinksRest;
use app\modules\api\models\PassangerCarsRest;
use app\modules\api\models\PassangerCarTreesRest;


/**
 * Class DetailsController
 * @package app\modules\api\controllers
 */
class DetailsController extends RestController
{

    /**
     * @var string
     */
    public $modelClass = 'app\modules\api\models\ArticleLinksRest';


    /**
     * Методы
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset(
            $actions['index'],
            $actions['view'],
            $actions['create'],
            $actions['update'],
            $actions['delete'],
            $actions['options']
        );

        return $actions;
    }

    /**
     * @param int $modification_id
     * @param int $section_id
     * @return array
     * @throws HttpException
     */
    public function actionList(int $modification_id, int $section_id)
    {
        $section = PassangerCarTreesRest::getSectionId($section_id, $modification_id);
        if($section === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        $modification = PassangerCarsRest::getModification($modification_id);
        if($modification === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        $ids[] = $section->number;
        $ids = array_merge($ids, PassangerCarTreesRest::getSectionAllIds($modification_id, $section_id));


        $query = (new \yii\db\Query())
            ->select([
                'art.id',
                'art.datasupplierarticlenumber as part_number',
                's.description as supplier_name',
                'prd.description as product_name',
                'art.supplierid',
                'art.linkageid as modification_id',
                'pds.nodeid as section_id',
                'tb_price.price as price'
            ])
            ->from('article_links as art')
            ->leftJoin('yii2_price tb_price', 'tb_price.article=art.datasupplierarticlenumber')
            ->leftJoin('passanger_car_pds pds', 'art.supplierid = pds.supplierid')
            ->leftJoin('suppliers s', 's.id = art.supplierid')
            ->leftJoin('passanger_car_prd prd', 'prd.id = art.productid')
            ->andWhere([
                'art.linkageid' => $modification_id,
                'pds.nodeid' => $ids
            ])
            ->andWhere('price is not null')
            ->andWhere('art.productid = pds.productid AND art.linkageid = pds.passangercarid')
            ->groupBy('art.id')
            ->orderBy('price asc');

        $count = $query->count();


        $pages = new Pagination([
            'totalCount' => $count,
            'defaultPageSize' => 15
        ]);

        $articles = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();


        $result = $articles;
        foreach ($articles as $key => $article) {
            $result[$key]['images'] = ArticleImagesRest::find()
                ->andWhere([
                    'DataSupplierArticleNumber' => $article['part_number'],
                    'supplierId' => $article['supplierid']
                ])
                ->all();
        }
        $result['count'] = $count;

        return $result;
    }

    /**
     * @param int $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView(int $id)
    {
        $article = ArticleLinksRest::find()
            ->select([
                'article_links.id',
                'article_links.datasupplierarticlenumber as part_number',
                's.description as supplier_name',
                'prd.description as product_name',
                'article_links.supplierid',
                'article_links.linkageid as modification_id',
                'pds.nodeid as section_id',
                'table_price.price as price'
            ])
            ->joinWith(['passangerCarPds as pds', 'suppliers as s', 'passangerCarPrd as prd', 'armtek as table_price'], false)
            ->andWhere('article_links.productid = pds.productid AND article_links.linkageid = pds.passangercarid')
            ->andWhere([
                //'article_links.datasupplierarticlenumber' => $code
                'article_links.id' => $id
            ])
            ->one();

        if($article === null) {
            throw new HttpException(404, Yii::t('app', 'Неправильный запрос'));
        }

        $result['view'] = [
            'id' => $article->id,
            'part_number' => $article->part_number,
            'supplier_name' => $article->supplier_name,
            'product_name' => $article->product_name,
            'supplierid' => $article->supplierid,
            'modification_id' => $article->modification_id,
            'section_id' => $article->section_id,
            'price' => $article->price,
        ];

        $result['attributes'] = ArticleAttributesRest::find()
            ->andWhere([
                'datasupplierarticlenumber' => $article->part_number,
                'supplierid' => $article->supplierid
            ])
            ->all();

        $result['images'] = ArticleImagesRest::find()
            ->andWhere([
                'DataSupplierArticleNumber' => $article->part_number,
                'supplierId' => $article->supplierid
            ])
            ->all();

        $crosses = ArticleLinksRest::find()
            ->select([
                'article_links.id',
                'article_links.datasupplierarticlenumber as part_number',
                's.description as supplier_name',
                'prd.description as product_name',
                'article_links.supplierid',
                'article_links.linkageid as modification_id',
                'pds.nodeid as section_id',
                'table_price.price as price'
            ])
            ->joinWith(['passangerCarPds as pds', 'suppliers as s', 'passangerCarPrd as prd', 'armtek as table_price'], false)
            ->andWhere('article_links.productid = pds.productid AND article_links.linkageid = pds.passangercarid')
            ->andWhere('article_links.id!=:id', [
                ':id' => $article->id
            ])
            ->andWhere([
                'article_links.linkageid' => $article->modification_id,
                'pds.nodeid' => $article->section_id
            ])
            ->andWhere('price is not null')
            ->groupBy('article_links.id')
            ->orderBy('price asc')
            ->all();


        $result['crosses'] = [];

        if(!empty($crosses)) {

            foreach ($crosses as $model) {
                $result['crosses'][] = [
                    'id' => $model->id,
                    'part_number' => $model->part_number,
                    'supplier_name' => $model->supplier_name,
                    'product_name' => $model->product_name,
                    'price' => $model->price,
                ];
            }

        }

        return $result;
    }
}
