<?php
namespace app\modules\api\controllers;


use app\models\Basket;
use app\models\Order;
use app\modules\api\components\RestController;
use app\modules\api\models\ArticleImagesRest;
use app\modules\api\models\ArticleLinksRest;
use Yii;


/**
 * Class BasketController
 * @package app\modules\api\controllers
 */
class BasketController extends RestController
{

    /**
     * @var string
     */
    public $modelClass = 'app\modules\api\models\BasketRest';


    /**
     * Методы
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();

        unset(
            $actions['index'],
            $actions['view'],
            $actions['delete'],
            $actions['options']
        );

        return $actions;
    }

    /**
     * @param $str
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionIndex($str)
    {
        if(Order::find()->where(['str' => $str])->exists()) {
            return [];
        } else {

            $baskets = Basket::find()->where(['str' => $str])
                ->all();

            $result = [];
            foreach ($baskets as $basket) {

                $article = ArticleLinksRest::find()
                    ->select([
                        'article_links.id',
                        'article_links.datasupplierarticlenumber as part_number',
                        's.description as supplier_name',
                        'prd.description as product_name',
                        'article_links.supplierid',
                        'article_links.linkageid as modification_id',
                        'pds.nodeid as section_id'
                    ])
                    ->joinWith(['passangerCarPds as pds', 'suppliers as s', 'passangerCarPrd as prd'], false)
                    ->andWhere('article_links.productid = pds.productid AND article_links.linkageid = pds.passangercarid')
                    ->andWhere([
                        'article_links.id' => $basket->article
                    ])
                    ->one();

                $result[] = [
                    'basket' => $basket,
                    'article' => [
                        'id' => $article->id,
                        'part_number' => $article->part_number,
                        'supplier_name' => $article->supplier_name,
                        'product_name' => $article->product_name,
                        'supplierid' => $article->supplierid,
                        'modification_id' => $article->modification_id,
                        'section_id' => $article->section_id,
                        'images' => ArticleImagesRest::find()
                            ->andWhere([
                                'DataSupplierArticleNumber' => $article->part_number,
                                'supplierId' => $article->supplierid
                            ])
                            ->all()
                    ]
                ];
            }

            return $result;
        }
    }


    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function actionStr()
    {
        return [
            'str' => Yii::$app->getSecurity()->generateRandomString()
        ];
    }
}
