<?php
namespace app\modules\api\models;

use app\models\PassangerCarTrees;

/*
SELECT id, description,
IF(
    EXISTS(
        SELECT * FROM passanger_car_trees t1
		INNER JOIN passanger_car_trees t2 ON t1.parentid=t2.id
        WHERE t2.parentid=0
        	AND t1.passangercarid=7218 LIMIT 1
    ), 1, 0
) AS havechild
FROM passanger_car_trees
WHERE passangercarid=7218 AND parentId=0
ORDER BY havechild
 */

/**
 * Class PassangerCarTreesRest
 * @package app\modules\api\models
 */
class PassangerCarTreesRest extends PassangerCarTrees
{

    /**
     * @brief Функция возвращает имя таблицы бд
     * @return string
     */
    public static function tableName()
    {
        return 'passanger_car_trees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['passangercarid', 'searchtreeid', 'number'], 'required'],
            [['passangercarid', 'searchtreeid', 'number', 'parentid'], 'integer'],
            [['description'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'passangercarid' => 'Passangercarid',
            'searchtreeid' => 'Searchtreeid',
            'number' => 'ID',
            'parentid' => 'Parentid',
            'description' => 'Description',
        ];
    }

    /**
     * @param $number
     * @param int $modification_id
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getSectionId($number, $modification_id = 117710)
    {
        return self::find()
            ->where([
                'number' => $number,
                'passangercarid' => $modification_id
            ])->one();
    }

    /**
     * @param $modification_id
     * @param $desciption
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getSectionName($modification_id, $description)
    {
        return self::find()
            ->where([
                'passangercarid' => $modification_id,
                'description' => $description
            ])->one();
    }

    /**
     * @param $modification_id
     * @param int $parent_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function section($modification_id, $parent_id = 0)
    {
        return self::find()
            ->andwhere([
                'passangercarid' => $modification_id,
                'parentid' => $parent_id
            ])
            ->orderBy('description asc')
            ->all();
    }

    /**
     * @brief 2 уровня секции по модификации
     * @param $id
     */
    public static function getSectionLevel2($id)
    {
        $models = self::section($id);

        $result = [];
        foreach ($models as $model) {
            $items = [
                'parent' => $model,
                'children' => self::section($id, $model->number)
            ];
            $result[] = $items;
        }

        return $result;
    }

    /**
     * @brief полное дерево секций по модификации
     * @param $id
     * @param int $parent_id
     * @return array
     */
    public static function getSectionAll($id, $parent_id = 0)
    {
        $models = self::section($id, $parent_id);

        $result = [];
        foreach ($models as $model) {
            $items = [
                'parent' => $model,
                'children' => self::getSectionAll($id, $model->number)
            ];
            $result[] = $items;
        }

        return $result;
    }

    /**
     * @param $id
     * @param int $parent_id
     * @return array
     */
    public static function getSectionAllIds($id, $parent_id = 0)
    {
        $models = self::section($id, $parent_id);

        $result = [];
        foreach ($models as $model) {
            $result[] = $model->number;
            $result = array_merge($result, self::getSectionAllIds($id, $model->number));
        }

        return $result;
    }
}
