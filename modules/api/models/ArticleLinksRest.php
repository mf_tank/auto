<?php
namespace app\modules\api\models;


use app\models\ArticleLinks;


/**
 * Class ManufacturersRest
 * @package app\modules\api\models
 */
class ArticleLinksRest extends ArticleLinks
{


    /**
     * @brief Функция возвращает имя таблицы бд
     * @return string
     */
    public static function tableName()
    {
        return 'article_links';
    }

}
