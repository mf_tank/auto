<?php
namespace app\modules\api\models;

use app\models\Models;
use app\models\PassangerCarAttributes;
use app\models\PassangerCars;

/**
 * Class PassangerCars
 * @package app\modules\api\models
 */
class PassangerCarsRest extends PassangerCars
{

    /**
     * @brief Функция возвращает имя таблицы бд
     * @return string
     */
    public static function tableName()
    {
        return 'passanger_cars';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'modelid'], 'integer'],
            [['canbedisplayed', 'haslink', 'isaxle', 'iscommercialvehicle', 'iscvmanufacturerid', 'isengine', 'ismotorbike', 'ispassengercar', 'istransporter'], 'string'],
            [['constructioninterval'], 'string', 'max' => 24],
            [['description'], 'string', 'max' => 128],
            [['fulldescription'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'canbedisplayed' => 'Canbedisplayed',
            'constructioninterval' => 'Constructioninterval',
            'description' => 'Description',
            'fulldescription' => 'Fulldescription',
            'haslink' => 'Haslink',
            'isaxle' => 'Isaxle',
            'iscommercialvehicle' => 'Iscommercialvehicle',
            'iscvmanufacturerid' => 'Iscvmanufacturerid',
            'isengine' => 'Isengine',
            'ismotorbike' => 'Ismotorbike',
            'ispassengercar' => 'Ispassengercar',
            'istransporter' => 'Istransporter',
            'modelid' => 'Modelid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassangerCarAttributes()
    {
        return $this->hasMany(PassangerCarAttributes::className(), ['passangercarid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Models::className(), ['id' => 'modelid']);
    }


    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getModifications($model_id)
    {
        return self::find()
            ->andwhere([
                'canbedisplayed' => 'True',
                'modelid' => $model_id,
                'ispassengercar' => 'True',
            ])
            ->with(['passangerCarAttributes'])
            ->orderBy('fulldescription asc')
            ->all();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getModification($id)
    {
        return self::find()
            ->andwhere([
                'id' => $id,
                'ispassengercar' => 'True',
            ])
            ->with(['passangerCarAttributes'])
            ->one();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getModificationName($model_id, $modification_description)
    {
        return self::find()
            ->andwhere([
                'canbedisplayed' => 'True',
                'ispassengercar' => 'True',
                'modelid' => $model_id,
                'description' => $modification_description
            ])
            ->joinWith(['passangerCarAttributes'])
            ->one();
    }

}
