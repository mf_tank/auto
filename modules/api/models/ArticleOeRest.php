<?php

namespace app\modules\api\models;


use app\models\ArticleOe;
use Yii;

/**
 * This is the model class for table "article_oe".
 *
 * @property int $supplierid
 * @property string $datasupplierarticlenumber
 * @property string $IsAdditive
 * @property string $OENbr
 * @property int $manufacturerId
 */
class ArticleOeRest extends ArticleOe
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article_oe';
    }

}
