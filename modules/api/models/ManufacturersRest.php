<?php
namespace app\modules\api\models;

use app\models\Manufacturers;

/**
 * Class ManufacturersRest
 * @package app\modules\api\models
 */
class ManufacturersRest extends Manufacturers
{

    /**
     * @brief Функция возвращает имя таблицы бд
     * @return string
     */
    public static function tableName()
    {
        return 'manufacturers';
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getBrands()
    {
        return self::find()
            ->andwhere([
                "canbedisplayed" => 'True',
                "ispassengercar" => 'True'
            ])
            ->orderBy('matchcode asc')
            ->all();
    }


    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getBrand($id)
    {
        return self::find()
            ->andwhere([
                "canbedisplayed" => 'True',
                "ispassengercar" => 'True',
                'id' => $id
            ])
            ->one();
    }

    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getBrandName($description)
    {
        return self::find()
            ->andwhere([
                "canbedisplayed" => 'True',
                "ispassengercar" => 'True',
                'description' => $description
            ])
            ->one();
    }

}
