<?php

namespace app\modules\api\models;

use app\models\Basket;
use Yii;

/**
 * Class BasketRest
 * @package app\modules\api\models
 */
class BasketRest extends Basket
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii2_basket';
    }

}
