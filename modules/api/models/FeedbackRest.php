<?php

namespace app\modules\api\models;

use Yii;
use app\models\Feedback;

/**
 * Class FeedbackRest
 * @package app\modules\api\models
 */
class FeedbackRest extends Feedback
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii2_feedback';
    }

}
