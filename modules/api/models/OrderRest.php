<?php

namespace app\modules\api\models;

use app\models\Order;
use Yii;

/**
 * Class BasketRest
 * @package app\modules\api\models
 */
class OrderRest extends Order
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii2_order';
    }

}
