<?php

namespace app\modules\api\models;

use app\models\ArticleAttributes;
use Yii;

/**
 * This is the model class for table "article_attributes".
 *
 * @property int $supplierid
 * @property string $datasupplierarticlenumber
 * @property int $id
 * @property string $description
 * @property string $displaytitle
 * @property string $displayvalue
 */
class ArticleAttributesRest extends ArticleAttributes
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article_attributes';
    }

}
