<?php
namespace app\modules\api\models;

use app\models\Models;

/**
 * Class ManufacturersRest
 * @package app\modules\api\models
 */
class ModelsRest extends Models
{

    /**
     * @brief Функция возвращает имя таблицы бд
     * @return string
     */
    public static function tableName()
    {
        return 'models';
    }


    /**
     * @param $make_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getModels($make_id)
    {
        return self::find()
            ->andwhere([
                'canbedisplayed' => 'True',
                'ispassengercar' => 'True',
                'manufacturerid' => $make_id
            ])
            ->orderBy('description asc')
            ->limit(99999)
            ->all();
    }

    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getModel($id)
    {
        return self::find()
            ->andwhere([
                'canbedisplayed' => 'True',
                'ispassengercar' => 'True',
                'id' => $id
            ])
            ->one();
    }


    /**
     * @param $brand_id
     * @param $description
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getModelName($brand_id, $description)
    {
        return self::find()
            ->andwhere([
                'canbedisplayed' => 'True',
                'ispassengercar' => 'True',
                'description' => $description,
                'manufacturerid' => $brand_id
            ])
            ->one();
    }
}
