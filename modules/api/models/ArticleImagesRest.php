<?php

namespace app\modules\api\models;

use app\models\ArticleImages;
use Yii;

/**
 * This is the model class for table "article_images".
 *
 * @property int $supplierId
 * @property string $DataSupplierArticleNumber
 * @property string $AdditionalDescription
 * @property string $Description
 * @property string $DocumentName
 * @property string $DocumentType
 * @property int $NormedDescriptionID
 * @property string $PictureName
 * @property string $ShowImmediately
 */
class ArticleImagesRest extends ArticleImages
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article_images';
    }

}
