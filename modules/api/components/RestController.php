<?php
namespace app\modules\api\components;

use Yii;
use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\filters\Cors;
use yii\web\Response;

/**
 * Class RestController
 * @package app\modules\api\controllers
 */
class RestController extends ActiveController
{

    /**
     * @var array
     */
    private $ipArray = [
        '185.253.217.230',
        '127.0.0.1',
    ];

    /**
     * @return mixed
     */
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => Cors::className(),
            ],
            /*
            'authenticator' => [
                'class' => QueryParamAuth::className(),
                'tokenParam' => 'hash'
            ],
            */
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ]
            ]

        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    /*public function beforeAction($action)
    {
        if(!in_array($_SERVER['SERVER_ADDR'], $this->ipArray)) {
            throw new \yii\web\ForbiddenHttpException('Доступ запрещен');
        }
        return parent::beforeAction($action);
    }*/

}
