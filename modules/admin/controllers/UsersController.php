<?php

namespace app\modules\admin\controllers;


use Yii;
use app\modules\admin\components\AdminController;
use app\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


/**
 * Class UsersController
 * @package app\modules\admin\controllers
 */
class UsersController extends AdminController
{

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'create',
                            'update',
                            'delete',
                            'view',
                            'options',
                            'search',
                            'change-password'
                        ],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Действие по умолчанию
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $searchModel = new User();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Действие создание
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new User(['scenario' => 'create']);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Успешно создано');

            return $this->redirect(['users/index']);
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Действие обновление
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        $model = User::getModel($id);
        $model->setScenario('update');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Успешно обновленно');

            return $this->redirect(['users/index']);
        }
        return $this->render('update', ['model' => $model]);
    }

    /**
     * Действие обновление
     * @return string|\yii\web\Response
     */
    public function actionChangePassword($id)
    {
        $model = User::getModel($id);
        $model->setScenario('change_password');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Успешно обновленно');

            return $this->redirect(['users/index']);
        }
        return $this->render('update', ['model' => $model]);
    }


    /**
     * Действие удаление
     * @return string|\yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = User::getModel($id);
        if($model->delete()) {
            Yii::$app->session->addFlash('success', 'Успешно удалено');
        }
        return $this->redirect(['users/index']);
    }
}
