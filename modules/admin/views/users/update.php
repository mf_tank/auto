<?php

use yii\bootstrap\Tabs;
use yii\helpers\Url;

$this->title = 'Редактирование';

echo Tabs::widget([
    'items' => [
        [
            'label' => 'Роли',
            'url' => Url::to(['users/update', 'id' => $model->id]),
            'active' => Url::to() == '/admin/users/update?id='.$model->id ? true : false,
            'content' => $this->render('form', ['model' => $model])
        ],
        [
            'label' => 'Изменить пароль',
            'url' => Url::to(['users/change-password', 'id' => $model->id]),
            'active' => Url::to() == '/admin/users/change-password?id='.$model->id ? true : false,
            'content' => $this->render('change_password', ['model' => $model])
        ],
    ],
]);

?>

