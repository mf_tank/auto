<?

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Изменить пароль';
?>


<?php $form = ActiveForm::begin([]); ?>

<?= $form->field($model, 'password_old')->passwordInput(); ?>

<?php $model->password = '';?>
<?= $form->field($model, 'password')->passwordInput(); ?>
<?= $form->field($model, 'password_repeat')->passwordInput(); ?>

<?= Html::submitButton('Изменить', [
    'class' => 'btn btn-primary'
]) ?>

<?php ActiveForm::end(); ?>


