<?php
/**
 * Грид пользователей
 * @var $this \yii\web\View
 * @var $dataProvider object DataProvider
 * @var $searchModel object User
 */

use yiister\gentelella\widgets\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\Button;

$this->title = Yii::t('app', 'Пользователи');


echo Html::a(Button::widget([
    'label' => 'Создать',
    'options' => ['class' => 'btn-primary'],
]),  ['users/create']);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'id',
        'username',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}{delete}'
        ]
    ]
]);

