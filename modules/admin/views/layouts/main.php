<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\admin\assets\AdminAsset;
use \yiister\gentelella\widgets\Menu;

AdminAsset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <?= Html::csrfMetaTags() ?>

        <?php
        $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => '/favicon.ico']);
        $this->registerLinkTag(['rel' => 'shortcut icon', 'type' => 'image/x-icon', 'href' => '/favicon.ico']);

        ?>

        <title>
            <?= Html::encode($this->title) ?>
        </title>

        <?php $this->head() ?>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="nav-<?= !empty($_COOKIE['menuIsCollapsed']) && $_COOKIE['menuIsCollapsed'] == 'true' ? 'sm' : 'md' ?>" >
        <?php $this->beginBody(); ?>
            <div class="container body">

                <div class="main_container">

                    <div class="col-md-3 left_col">
                        <div class="left_col scroll-view">

                            <div class="navbar nav_title" style="border: 0;">
                                <a href="/" class="site_title"><i class="fa fa-paw"></i>
                                    <span>
                                        <? echo Yii::t('admin', 'Control panel') ?>
                                    </span>
                                </a>
                            </div>
                            <div class="clearfix"></div>

                            <br/>

                            <!-- sidebar menu -->
                            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                                <div class="menu_section">
                                    <?=
                                    Menu::widget(
                                        [
                                            "items" => [
                                                [
                                                    "label" => Yii::t("admin", 'Пользователи'),
                                                    "url" => ["/admin/users/index"],
                                                ],
                                                [
                                                    "label" => "Widgets",
                                                    "icon" => "th",
                                                    "url" => "#",
                                                    "items" => [
                                                        ["label" => "Menu", "url" => ["site/menu"]],
                                                        ["label" => "Panel", "url" => ["site/panel"]],
                                                    ],
                                                ],

                                            ],
                                        ]
                                    )
                                    ?>
                                </div>

                            </div>
                            <!-- /sidebar menu -->
                        </div>
                    </div>


                    <!-- top navigation -->
                    <div class="top_nav">

                        <div class="nav_menu">
                            <nav class="" role="navigation">
                                <div class="nav toggle">
                                    <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                                </div>

                                <?php if(!Yii::$app->user->isGuest) {?>
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="">
                                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <?php echo Yii::$app->user->identity->username;?>
                                                <span class=" fa fa-angle-down"></span>
                                            </a>
                                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                                <li>
                                                    <a href="<?php echo Url::to(['site/logout']);?>" data-method="post">
                                                        <i class="fa fa-sign-out pull-right"></i>
                                                        Выход
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                <?php }?>
                            </nav>
                        </div>

                    </div>
                    <!-- /top navigation -->

                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="clearfix"></div>

                        <?php echo $this->render('_flash');?>

                        <?= $content ?>
                    </div>
                    <!-- /page content -->
                    <!-- footer content -->
                    <footer>
                        <div class="pull-right"></div>
                        <div class="clearfix"></div>
                    </footer>
                    <!-- /footer content -->
                </div>

            </div>

            <div id="custom_notifications" class="custom-notifications dsp_none">
                <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group"></ul>
                <div class="clearfix"></div>
                <div id="notif-group" class="tabbed_notifications"></div>
            </div>
            <!-- /footer content -->
        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>
