<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

/**
 * Class AdminAsset
 * @package app\modules\admin\assets
 */
class AdminAsset extends AssetBundle
{
    //public $basePath = '@webroot';
    //public $baseUrl = '@web';


    public $css = [

    ];

    public $js = [

    ];

    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
        'yiister\gentelella\assets\Asset'
    ];
}
