<?php

use yii\db\Migration;

/**
 * Class m191208_115210_tbl_basket
 */
class m191208_115210_tbl_basket extends Migration
{
    /**
     * Накат миграции
     * @return void
     */
    public function up()
    {
        $this->createTable('{{%basket}}', [
            'id' => $this->primaryKey(),
            'str' => $this->string()->notNull(),
            'article' => $this->string()->notNull(),
            'section_id' => $this->string()->notNull(),
            'modification_id' => $this->string()->notNull(),
            'count' => $this->integer()->notNull(),
            'price' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->createIndex('basket_hash', '{{%basket}}', 'str');

        $this->createIndex('basket_4', '{{%basket}}',
            ['str', 'article', 'section_id', 'modification_id'],
            true
        );
    }

    /**
     * Откат миграции
     * @return void
     */
    public function down()
    {
        $this->dropTable('{{%basket}}');
    }
}
