<?php

use yii\db\Migration;

/**
 * Class m191209_150644_price
 */
class m191209_150644_price extends Migration
{

    /**
     * Накат миграции
     * @return void
     */
    public function up()
    {
        $this->createTable('{{%price}}', [
            'id' => $this->primaryKey(),
            'article' => $this->string()->notNull(),
            'brand' => $this->string()->notNull(),
            'code' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'price' => $this->money()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->createIndex('price_article', '{{%price}}', 'article', true);


        $this->createTable('{{%store}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');


        $this->createTable('{{%price_store}}', [
            'id' => $this->primaryKey(),
            'price_id' => $this->integer()->notNull(),
            'store_id' => $this->integer()->notNull(),
            'count' => $this->integer()->notNull(),
            'date' => $this->dateTime()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer(),
            'updater_id' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'price_store_price',
            '{{%price_store}}',
            'price_id',
            '{{%price}}',
            'id'
        );
        $this->addForeignKey(
            'price_store_store',
            '{{%price_store}}',
            'store_id',
            '{{%store}}',
            'id'
        );
        $this->createIndex(
            'price_store2',
            '{{%price_store}}',
            'price_id, store_id',
            true
        );
    }

    /**
     * Откат миграции
     * @return void
     */
    public function down()
    {

        $this->dropTable('{{%price_store}}');
        $this->dropTable('{{%price}}');
        $this->dropTable('{{%store}}');
    }
}
