<?php


use yii\db\Migration;
use app\models\User;

/**
 * Class m150331_072245_tbl_user
 */
class m150331_072245_tbl_user extends Migration
{

    /**
     * Накат миграции
     * @return void
     */
    public function up()
    {


        $auth = Yii::$app->authManager;
        $guest = $auth->createRole(User::ROLE_GUEST);
        $auth->add($guest);

        $admin = $auth->createRole(User::ROLE_ADMIN);
        $auth->add($admin);

        $auth->addChild($admin, $guest);
        $auth->assign($admin, 1);


        $user = new User();
        $user->username = 'skugarev.andrey@gmail.com';
        $user->password = '111';
        $user->is_active = 1;
        $user->save();
    }

    /**
     * Откат миграции
     * @return void
     */
    public function down()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAllRoles();
        $auth->removeAll();
        $this->dropTable('{{%user}}');
    }
}
