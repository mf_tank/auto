<?php
namespace app\commands;

use app\models\Price;
use app\models\PriceStore;
use app\models\Store;
use Yii;
use yii\console\Controller;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;


/**
 * Class ImportController
 * @package app\commands
 */
class  ImportController extends Controller
{

    /**
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Reader\Exception\ReaderNotOpenedException
     */
    public function actionIndex()
    {
        $files = [
            Yii::getAlias('@app/web/import/').'1.xlsx',
            Yii::getAlias('@app/web/import/').'2.xlsx'
        ];

        foreach ($files as $file) {

            if(file_exists($file)) {

                $reader = ReaderEntityFactory::createXLSXReader();
                $reader->open($file);

                foreach ($reader->getSheetIterator() as $sheet) {
                    foreach ($sheet->getRowIterator() as $key => $row) {
                        $cells = $row->getCells();
                        $columns = [];
                        foreach ($cells as $cell) {
                            $columns[] = $cell->getValue();
                        }

                        if (!empty($columns)) {
                            $price = Price::find()
                                ->where(['article' => $columns[0]])->one();
                            if (!$price) {
                                $price = new Price();
                            }

                            $price->article = $columns[0];
                            $price->brand = $columns[1];
                            $price->code = $columns[2];
                            $price->name = $columns[3];
                            $price->price = (float)$columns[8]*2;
                            $price->save();


                            $store = Store::find()
                                ->where(['name' => $columns[6]])
                                ->one();
                            if (!$store) {
                                $store = new Store();
                            }

                            $store->name = $columns[6];
                            $store->save();

                            PriceStore::deleteAll(['price_id' => $price->id]);
                            $priceStore = new PriceStore();
                            $priceStore->price_id = $price->id;
                            $priceStore->store_id = $store->id;
                            $priceStore->count = $columns[4];
                            $priceStore->date = date('Y-m-d H:i:s', strtotime($columns[7]));
                            $priceStore->save();
                        }
                    }
                }

                $reader->close();
            }
        }
    }
}
