<?php

namespace app\controllers;

use app\components\BaseController;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\HttpException;
use yii\web\Response;
use yii\filters\VerbFilter;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends BaseController
{

    /**
     * @var string
     */
    public $layout = 'auth';


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(['/profile']);
        }

        $model = new User(['scenario' => 'login']);
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/profile']);
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    /*
    public function actionReg()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(['/']);
        }

        $model = new User(['scenario' => 'registration']);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->mailer->compose('reg', ['token' => $model->access_token])
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo($model->username)
                ->setSubject('Регистрация')
                ->send();

            Yii::$app->session->addFlash('success', 'Вам выслано на почту письмо с подтверждением');

            return $this->refresh();
        }

        return $this->render('reg', [
            'model' => $model,
        ]);
    }
    */

    /**
     * @param $token
     * @throws HttpException
     */
    public function actionSuccess($token)
    {
        $model = User::find()->where(['access_token' => $token])
            ->one();

        if(!$model) {
            throw new HttpException(404, Yii::t('app', 'Нет такой страницы'));
        }

        $model->is_active = 1;
        $model->save(false);

        Yii::$app->session->addFlash('success', 'Почта успешно подтвержденна теперь вы можете авторизоваться');

        return $this->render('success');
    }

    /**
     * @return string|Response
     */
    public function actionForget()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(['/']);
        }

        $model = new User(['scenario' => 'forget']);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $user = $model->getUser();
            $password = Yii::$app->getSecurity()->generateRandomString();
            $user->password = Yii::$app->getSecurity()->generatePasswordHash($password);
            $user->save(false);

            Yii::$app->mailer->compose('forget', [
                'password' => $password,
                'username' => $model->username
            ])
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo($model->username)
                ->setSubject('Ваш новый пароль')
                ->send();

            Yii::$app->session->addFlash('success', 'Вам выслан новый пароль на почту');

            return $this->redirect(['/']);
        } else {
            return $this->render('forget', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
